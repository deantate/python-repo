# 1,000 images
# 234, 332, rest in folders
# blue: 20%
# purple: 25%
# yellow: 1
# orange: 25%
# red: 30%

import random, os, sys
import shutil

def add_zeros(name):
    tmp = name
    for i in range(0, 4 - len(name)):
        tmp = '0' + tmp
    return tmp

im = ['blue', 'purple', 'yellow', 'red']
dirs = ['DAMC001', 'DAMC002', 'DAMC003']


dirs_iter = 0
for i in range(1,3001):
    if(i == 233 or i == 784):
        dirs_iter += 1
    r = random.randint(0, 3)
    src_image = im[r] + '.png'
    dst_image = "IMG" + add_zeros(str(i)) + ".png"
    dst_path = os.path.join(dirs[dirs_iter],dst_image)
    shutil.copyfile(src_image, dst_path)
    

